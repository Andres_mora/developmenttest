<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Creacion de las relaciones de la tabla tramites
        Schema::table('tramites', function($table)
        {
            $table->foreign('id_persona')
                    ->references('id')
                    ->on('personas')
                    ->onUpdate('cascade');

            $table->foreign('id_empleado')
                    ->references('id')
                    ->on('empleados')
                    ->onUpdate('cascade');
        });

        
        // Creacion de las relaciones de la tabla Radicado
        Schema::table('radicados', function($table)
        {
            $table->foreign('id_temas')
                    ->references('id')
                    ->on('temas')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
