function MostrarEdit(btn)
{
	var route = '/Radicacion/'+btn.value+'/edit';

	// Validacion para mostrar los datos de la operacion
	$.get(route, function(res)
	{
        $('#numero_radicado_edit').val(res.id);
        $('#fecha_edit').val(res.fecha);
        $('#titulo_edit').val(res.titulo);
        $('#temas_edit').val(res.id_temas);
	});
}

function revisar_edit(elemento)
{
    if(elemento.value=='')
    {
        alertify.set('notifier','position', 'top-center');
        alertify.error('ERROR EL CAMPO "'+ elemento.name + '" ES REQUERIDO', 5);
    }
}


function edit()
{
    var numero_radicado = $('#numero_radicado_edit').val();
    var fecha = $('#fecha_edit').val();
    var titulo = $('#titulo_edit').val();
    var temas = $('#temas_edit').val();

	var route = '/Radicacion/'+numero_radicado+'';
	var token = $('#token').val();

    if(numero_radicado == '' || fecha == '' || titulo == '' || temas == null)
    {
        alertify.set('notifier','position', 'top-center');
        alertify.error('LOS CAMPOS OBLIGATORIOS (*) NO FUERON INGRESADOS', 5);
    }
    else
    {
        $.ajax(
        {
            url: route,
            headers:
            {
                'X-CSRF-TOKEN': token
            },
            type: 'PUT',
            dataType: 'json',
            data:
            {
                fecha: fecha,
                titulo: titulo,
                temas: temas,
            },
            success:function()
            {
                alertify.set('notifier','position', 'top-center');
                alertify.success('EL RADICADO SE A MODIFICADO CORRECTAMENTE', 8);
                location.reload();
            },
            error: function (data)
            {			
                alertify.set('notifier','position', 'top-center');
                alertify.error('POR FAVOR REVISA LA INFORMACION INSERTADA', 5);
                alertify.error('ALGO HA SALIDO MAL ...', 5);
                console.log('Error:', data);
            }
        });
    }
};