// Revisar los campos obligatorios
function revisar(elemento)
{
    if(elemento.value=='')
    {
        alertify.set('notifier','position', 'top-center');
        alertify.error('ERROR EL CAMPO "'+ elemento.name + '" ES REQUERIDO', 5);
    }
}

function create()
{
    var numero_radicado = $('#numero_radicado_new').val();
    var fecha = $('#fecha_new').val();
    var titulo = $('#titulo_new').val();
    var temas = $('#temas_new').val();

	var route = '/Radicacion';
	var token = $('#token').val();

    if(numero_radicado == '' || fecha == '' || titulo == '' || temas == null)
    {
        alertify.set('notifier','position', 'top-center');
        alertify.error('LOS CAMPOS OBLIGATORIOS (*) NO FUERON INGRESADOS', 5);
    }
    else
    {
        $.ajax(
        {
            url: route,
            headers:
            {
                'X-CSRF-TOKEN': token
            },
            type: 'POST',
            dataType: 'json',
            data:
            {
                numero_radicado: numero_radicado,
                fecha: fecha,
                titulo: titulo,
                temas: temas,
            },
            success:function()
            {
                alertify.set('notifier','position', 'top-center');
                alertify.success('LA RADICACION PARA CON EL NUMERO: ' + numero_radicado + ', SE HA REGISTRADO CORRECTAMENTE', 5);
                location.reload();
            },
            error: function (data)
            {
                alertify.set('notifier','position', 'top-center');
                alertify.error('EL NUMERO DEL RADICADO, YA SE ENCUENTRA REGISTRADO EN EL SISTEMA', 5);
                alertify.error('ALGO HA SALIDO MAL ...', 5);
                console.log('Error:', data);
            }
        });
    }
};