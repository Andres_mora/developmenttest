<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Radicado;
use App\Models\Temas;

class ControllerRadicado extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $numero_radicado = null;
        $alerta = 'BIENVENIDO A RADICADOS';

        $temas = Temas::orderBy('nombre')
                        ->get();

        $radicado = Radicado::orderBy('titulo')
                                ->paginate(10);

        return view('Radicado.list')->with(compact('radicado', 'temas', 'numero_radicado', 'alerta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Radicado.Modals.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $radicado = new Radicado;
            $radicado->id = $request->numero_radicado;
            $radicado->fecha = $request->fecha;
            $radicado->titulo = $request->titulo;
            $radicado->id_temas = $request->temas;
            $radicado->save();

            return response()->json([
                'mensaje' => 'Datos registrados correctamente',
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $radicado = Radicado::find($id);

        return response()->json(
            $radicado->toArray(),
        );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->ajax())
        {
            $radicado =Radicado::find($id);
            $radicado->fecha = $request->fecha;
            $radicado->titulo = $request->titulo;
            $radicado->id_temas = $request->temas;
            $radicado->save();

            return response()->json([
                'mensaje' => 'Datos actualizads correctamente',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Radicado::find($id)->delete();

        return back();
    }

    /**
     * Funcion para generar las consultas
     */

     public function search(Request $request)
     {
        $numero_radicado = $request->numero_radicado;        
        $alerta = null;

        $temas = Temas::orderBy('nombre')
                        ->get();

        if($numero_radicado != null)
        {
            $radicado = Radicado::where('id', 'like', '%'.$numero_radicado.'%')
                                    ->orderBy('titulo')
                                    ->paginate(10);

            $msj = 'para el radicado con el numero: '.$numero_radicado;
        }
        else
        {
            $radicado = Radicado::orderBy('titulo')
                                ->paginate(10);

                                
            $msj = 'al buscar en general, sin numero de radicado';
        }
        
        if($radicado->count() == 0)
        {
            $alerta = 'No se encontraron registros en el sistema';
        }
        else
        {
            $alerta = 'Se encontraron registros '.$msj ;
        }

        return view('Radicado.list')->with(compact('radicado', 'temas', 'numero_radicado', 'alerta'));
     }
}
