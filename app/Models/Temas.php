<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Temas extends Model
{
    protected $table = 'temas';
    protected $fillable = ['nombre'];
    protected $guarded = ['id'];
}
