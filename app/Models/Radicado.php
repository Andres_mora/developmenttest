<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Radicado extends Model
{
    protected $table = 'radicados';
    protected $fillable = ['fecha', 'titulo', 'id_temas'];
    protected $guarded = ['id'];

    public function temas()
    {
        return $this->belongsTo('App\Models\Temas', 'id_temas', 'id');
    }
}
