<!-- Modal -->
<div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <!-- Colocamos un input oculto con el token -->
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          <h4 class="modal-title" id="myModalLabel"><center>{{ __('EDITAR RADICADO')}}</center></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>{{ __('NUMERO DE RADICADO*') }}</label>
                    <input name="NUMERO DE RADICADO" type="number" class="form-control" id="numero_radicado_edit" onblur="revisar_edit(this);" disabled>
                </div>
                <div class="col-md-6 mb-3">
                    <label>{{ __('FECHA*') }}</label>
                    <input name="FECHA" type="datetime" class="form-control" id="fecha_edit" onblur="revisar_edit(this);" step="1" min="2013-01-01T00:00Z" max="2013-12-31T12:00Z">
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>{{ __('TITULO*') }}</label>
                    <input name="TITULO" type="text" class="form-control" id="titulo_edit" onblur="revisar_edit(this);" style="text-transform:uppercase;"onkeyup="javascript:this.value=this.value.toUpperCase();">
                </div>
                <div class="col-md-6 mb-3">
                    <label>{{ __('TEMAS*') }}</label>
                    <select name="TEMAS" id="temas_edit" class="form-control" onblur="revisar_edit(this);">
                        <option value=" " selected disabled>{{ __('SELECCIONE UNA OPCION') }}</option>
                        @foreach ($temas as $tema)
                            <option value="{{ $tema->id }}">{{ $tema->nombre }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">{{ __('CANCELAR') }}</button>
          <button type="button" class="btn btn-outline-success" onclick="edit();">{{ __('GUARDAR CAMBIOS') }}</button>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="{{ asset('/js/Radicado/update.js') }}"></script>