@extends('layouts.app')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom container">
        <h1 class="h2">{{ __('RADICADOS') }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <button class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#ModalNew">
                <img src="{{ asset('feather/plus-circle.svg') }}">
                {{ __('NUEVO RADICADO') }}
            </button>
        </div>
    </div>
    <div class="container">
        <form action="{{ url('/Search/Radicacion') }}" method="GET">
            <div class="jumbotron" style="background-color: #fff; border-style: groove; border-color: #E9E9E9; border-width:5px;">
                <div class="panel panel-header">
                    <div class="panel-body">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label>{{ __('NUMERO DE RADICADO') }}</label>
                                <input type="number" name="numero_radicado" id="numero_radicado" class="form-control" value="{{ $numero_radicado }}">
                                <input type="hidden" value="{{ $alerta }}" id="alerta">
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <br>
                </div>
                <div class="btn-toolbar mb-2 mb-md-0">                    
                    <button type="button" class="btn btn-outline-primary" title="LIMPIAR FILTROS DE BUSQUEDA" onclick="Clear();">
                        <img src="{{ asset('feather/trash.svg') }}">
                    </button>
                    <button type="submit" class="btn btn-outline-success">                        
                        <img src="{{ asset('feather/search.svg') }}">
                        {{ __('BUSCAR') }}
                    </button>
                </div>
            </div>
        </form>
        <h4>
            HAY {{ $radicado->total() }}
            @if($radicado->total() == '1')
                {{ ('RADICADO') }}
            @else        
                {{ ('RADICADOS') }}
            @endif
            EN TOTAL
        </h4>
        <div class="table-responsive">
            <table class="table table-hover table-sm">
                <thead>
                    <tr class="table-gea">
                        <th><center>{{ __('NUMERO DE RADICADO') }}</center></th>
                        <th><center>{{ __('FECHA') }}</center></th>
                        <th><center>{{ __('TITULO') }}</center></th>
                        <th><center>{{ __('TEMAS') }}</center></th>
                        <th><center>{{ __('ACCIÓNES') }}</center></th>
                    </tr>
                </thead>
                <tbody id="tableStatus">
                    @foreach ($radicado as $radicados)
                        <tr>
                            <td class="hoverable"><center>{{$radicados->id}}</center></td>
                            <td class="hoverable"><center>{{$radicados->fecha}}</center></td>
                            <td class="hoverable"><center>{{$radicados->titulo}}</center></td>
                            <td class="hoverable"><center>{{$radicados->temas->nombre}}</center></td>
                            <td class="hoverable">
                                <center>
                                    <button value="{{ $radicados->id }}" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#ModalUpdate" id="UpdateStaus" onclick="MostrarEdit(this);" title="EDITAR">
                                        <img src="{{ asset('feather/edit.svg') }}">
                                    </button>
                                    <a href="/Radicacion/Delete/{{ $radicados->id }}" class="btn btn-sm btn-outline-danger" onclick="return confirm('¿ESTA SEGURO DE ELIMINAR EL RADICADO?');" title="ELIMINAR RADICADO">
                                        <img src="{{ asset('feather/trash-2.svg') }}">
                                    </a>
                                </center>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="pull-right">
            {{ $radicado->appends(Request::capture()->except('page'))->render() }}
        </div>
    </div>
    {{ csrf_field() }}
    @include('Radicado/Modals.new')
    @include('Radicado/Modals.update')
    <script type="text/javascript" src="{{ asset('/js/Radicado/list.js') }}"></script>
@endsection
        